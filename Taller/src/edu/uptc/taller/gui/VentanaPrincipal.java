package edu.uptc.taller.gui;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import edu.uptc.taller.logic.Cola;
import edu.uptc.taller.logic.HiloGestion;
import edu.uptc.taller.logic.HiloTurno;
import edu.uptc.taller.logic.Modulo;
import edu.uptc.taller.logic.Nodo;

public class VentanaPrincipal extends JFrame {

    private HiloGestion gestion;
    private Panel panel;
    private Eventos eventos;
    private HiloTurno hilo;

    public VentanaPrincipal() {
	super();
	setTitle("Simulación de atención al cliente");
	setSize(700, 720);
	setLocationRelativeTo(null);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setResizable(false);
	inicializarComponentes();
	iniciarHilos();
    }

    public void inicializarComponentes() {
	Modulo m1 = new Modulo("Módulo A");
	Modulo m2 = new Modulo("Módulo B");
	Modulo m3 = new Modulo("Módulo C");
	Cola turnos = new Cola();
	gestion = new HiloGestion(m1, m2, m3, turnos);
	eventos = new Eventos(this);
	panel = new Panel(this);
	hilo = new HiloTurno(gestion.getTurnos());
	add(panel);
    }

    private void llenarFila() {
	Nodo aux = gestion.getTurnos().getHead();
	JTextArea t = new JTextArea();
	Font font = new Font("a", Font.BOLD, 13);
	t.setFont(font);
	String lista = "Cola:\n";
	while (aux != null) {
	    lista += "Turno: " + aux.getTurno().getTurno() + " Cliente: "
		    + aux.getTurno().getCliente() + "\n";
	    aux = aux.getNext();
	}
	t.setText(lista);
	t.setEditable(false);
	panel.getFila().removeAll();
	panel.getFila().add(t);
	panel.updateUI();
    }

    public void anadiraCola() {
	hilo.run();
    }

    private void iniciarHilos() {
	llenarFila();
	Thread monitor = new Thread(new Runnable() {

	    @Override
	    public void run() {
		try {
		    Thread.sleep(1000);
		    gestion.start();
		    while (true) {
			Thread.sleep(20);
			llenarFila();
			boolean estadom1 = gestion.getModulos().get(0)
				.isOcupado();
			boolean estadom2 = gestion.getModulos().get(1)
				.isOcupado();
			boolean estadom3 = gestion.getModulos().get(2)
				.isOcupado();
			if (estadom1 && estadom2 && estadom3) {
			    panel.getEstado().setText(
				    "Módulos ocupados, esperando...");
			} else {
			    panel.getEstado().setText("");
			}
			if (!estadom1 && !estadom2 && !estadom3) {
			    panel.getEstado().setText(
				    "Esperando por nuevos clientes...");
			}
			if (estadom1 == true) {
			    panel.getEstadom1().setText("OCUPADO");
			    panel.getM1info().setText(
				    "Turno: "
					    + gestion.getModulos().get(0)
						    .getCliente().getTurno()
					    + "\nCliente: "
					    + gestion.getModulos().get(0)
						    .getCliente().getCliente());
			} else {
			    panel.getEstadom1().setText("    LIBRE");
			    panel.getM1info().setText("Esperando Cliente...");
			}
			if (estadom2 == true) {
			    panel.getEstadom2().setText("OCUPADO");
			    panel.getM2info().setText(
				    "Turno: "
					    + gestion.getModulos().get(1)
						    .getCliente().getTurno()
					    + "\nCliente: "
					    + gestion.getModulos().get(1)
						    .getCliente().getCliente());
			} else {
			    panel.getEstadom2().setText("    LIBRE");
			    panel.getM2info().setText("Esperando Cliente...");
			}
			if (estadom3 == true) {
			    panel.getEstadom3().setText("OCUPADO");
			    panel.getM3info().setText(
				    "Turno: "
					    + gestion.getModulos().get(2)
						    .getCliente().getTurno()
					    + "\nCliente: "
					    + gestion.getModulos().get(2)
						    .getCliente().getCliente());
			} else {
			    panel.getEstadom3().setText("    LIBRE");
			    panel.getM3info().setText("Esperando Cliente...");
			}
		    }
		} catch (InterruptedException e) {
		    // TODO Bloque catch generado automáticamente
		    e.printStackTrace();
		}

	    }
	});
	monitor.start();
    }

    public HiloGestion getGestion() {
	return gestion;
    }

    public Panel getPanel() {
	return panel;
    }

    public Eventos getEventos() {
	return eventos;
    }

    public static void main(String[] args) {
	VentanaPrincipal ventana = new VentanaPrincipal();
	ventana.setVisible(true);
    }

}
