package edu.uptc.taller.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Eventos implements ActionListener{
    
    private VentanaPrincipal ventana;
    public static final String ADD = "Añadir a la cola";
    
    

    public Eventos(VentanaPrincipal ventana) {
	super();
	this.ventana = ventana;
    }



    @Override
    public void actionPerformed(ActionEvent e) {
	if (e.getActionCommand().equals(ADD)) {

	    ventana.anadiraCola();

	}
	
	
    }

}
