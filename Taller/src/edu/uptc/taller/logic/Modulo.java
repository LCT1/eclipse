package edu.uptc.taller.logic;

import java.util.Random;

public class Modulo extends Thread {

    private String nombre;
    private Turno cliente;
    private boolean ocupado;

    public Modulo(String nombre) {
	super();
	this.nombre = nombre;
	ocupado = false;
	cliente = null;
    }



    public Turno getCliente() {
        return cliente;
    }



    public void setCliente(Turno cliente) {
        this.cliente = cliente;
    }



    public boolean isOcupado() {
	return ocupado;
    }

    public void setOcupado(boolean ocupado) {
	this.ocupado = ocupado;
    }

    public String getNombre() {
	return nombre;
    }

    @Override
    public void run() {
	while (true) {
	    try {
		sleep(0);

		if (ocupado == true) {

		    Random rnd = new Random();
		    int numero = rnd.nextInt(19000);
		    sleep(numero+1000);
		    setOcupado(false);

		}
	    } catch (InterruptedException e1) {
		// TODO Bloque catch generado automáticamente
		e1.printStackTrace();
	    }
	}

    }

}
