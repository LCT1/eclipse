package edu.uptc.taller.logic;

public class Cola {

    private Nodo head;

    public Cola() {
	super();
    }

    public  void add(Turno turno) {

	Nodo aux;
	Nodo newNode = new Nodo(turno);

	if (isEmpty()) {
	    head = newNode;
	}

	else {

	    aux = head;

	    while (aux.getNext() != null) {

		aux = aux.getNext();

	    }
	    aux.setNext(newNode);

	}
    }

    public int getWeight(){
	int cuenta = 0;
	Nodo  aux = head;
	while (aux != null) {
	    cuenta += 1;
	    aux = aux.getNext();
	}
	return cuenta;
    }
    
    public void removeHead(){
	head = head.getNext();
    }

    public boolean isEmpty() {

	if (head == null) {
	    return true;
	}

	return false;

    }

    public Nodo getHead() {
	return head;
    }


    public void setHead(Nodo head) {
	this.head = head;
    }

    public void listNodes() {

	Nodo aux = head;
	String next;

	if (isEmpty()) {
	    System.out.println("Cola Vacía");
	} else {
	    do {
		
		if (aux.getNext() != null) {
		    next = aux.getNext().getTurno().toString();
		} else {
		    next = "null";
		}

		System.out.println(aux.getTurno().toString() + "  --  " + "next= "
			+ next);
		aux = aux.getNext();
	    } while (aux != null);
	}

    }

}
