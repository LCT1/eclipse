package edu.uptc.taller.logic;

import java.util.ArrayList;

public class HiloGestion extends Thread {

    private Cola turnos;
    private ArrayList<Modulo> modulos;

    public HiloGestion(Modulo m1, Modulo m2, Modulo m3, Cola turnos) {
	super();
	this.turnos = turnos;
	modulos = new ArrayList<Modulo>();
	modulos.add(m1);
	modulos.add(m2);
	modulos.add(m3);
    }

    private Modulo getModuloDisponible() {
	for (int i = 0; i < 3; i++) {
	    if (modulos.get(i).isOcupado() == false) {
		return modulos.get(i);
	    }
	}
	return null;
    }

    private void iniciarHilos() {
	for (int i = 0; i < modulos.size(); i++) {
	    modulos.get(i).start();
	}
    }

    public Cola getTurnos() {
	return turnos;
    }

    public ArrayList<Modulo> getModulos() {
	return modulos;
    }

    @Override
    public void run() {
	iniciarHilos();
	while (true) {
	    try {
		Modulo disponible = getModuloDisponible();
		if (disponible != null) {
		    sleep(500);
		    if (!turnos.isEmpty()) {
			Turno cliente = turnos.getHead().getTurno();
			disponible.setCliente(cliente);
			turnos.removeHead();
			disponible.setOcupado(true);
		    }

		} else {
		    sleep(2000);
		}
	    } catch (InterruptedException e) {
		// TODO Bloque catch generado automáticamente
		e.printStackTrace();
	    }
	}
    }

}
