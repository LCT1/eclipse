package edu.uptc.taller.logic;

public class Nodo {
    
    private Turno turno;
    private Nodo next;

    public Nodo(Turno turno) {
	super();
	this.turno = turno;
    }

    

    public Turno getTurno() {
        return turno;
    }



    public void setTurno(Turno turno) {
        this.turno = turno;
    }



    public Nodo getNext() {
        return next;
    }

    public void setNext(Nodo next) {
        this.next = next;
    }

    
    
    
    
    
    

}
